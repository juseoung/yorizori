import React, { Suspense } from 'react';

import Spinner from './pages/Spinner';

const Home = React.lazy(() => import('../src/pages/Home'));

const App = () => {
  return (
    <>
      <Suspense fallback={<Spinner />}>
        <Home />
      </Suspense>
    </>
  );
};

export default App;
