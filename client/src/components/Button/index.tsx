import React from 'react';

interface ButtonProps {
  variant?: 'contained' | 'lined' | 'text';
  size?: 'tiny' | 'small' | 'medium' | 'large' | 'big';
  color?: 'primary' | 'secondary';
  text: React.ReactNode;
}

const VARIANT_OPTION = {
  contained: '',
  lined: '',
  text: '',
};
const SIZE_OPTION = {
  tiny: '',
  small: '',
  medium: 'text-xs py-1 px-2',
  large: '',
  big: '',
};
const COLOR_OPTION = {
  primary: 'bg-primary-100',
  secondary: 'bg-secondary-100',
};

const Button = ({ variant, size, color, text }: ButtonProps) => {
  const variantClassName = variant && color ? VARIANT_OPTION[variant] : '';
  const sizeClassName = size ? SIZE_OPTION[size] : '';
  const colorClassName = color ? COLOR_OPTION[color] : '';
  return (
    <button
      className={`${variantClassName} ${sizeClassName} ${colorClassName}`}
    >
      {text}
    </button>
  );
};

Button.defaultProps = {
  variant: 'contained',
  size: 'tiny',
  color: 'primary',
  text: '버튼',
};

export default Button;
