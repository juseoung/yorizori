import React from 'react';

interface ContainerProps {
  bgColor?: 'white' | 'gray';
  className?: string;
  children: React.ReactNode;
}

const BG_OPTION = {
  white: 'bg-white',
  gray: 'bg-gray-60',
};

const Container = ({ bgColor, className, children }: ContainerProps) => {
  const bgClassName = bgColor ? BG_OPTION[bgColor] : '';

  return (
    <div className={`${bgClassName} ${className} h-screen px-2`}>
      {children}
    </div>
  );
};

Container.defaultProps = {
  bgColor: 'white',
};

export default Container;
