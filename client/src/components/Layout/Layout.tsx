import React from 'react';
import Header from './Header';
import Container from './Container';
import Footer from './Footer';

interface LayoutProps {
  header?: boolean;
  footer?: boolean;
  children: React.ReactNode;
}

const Layout = ({ header, footer, children }: LayoutProps) => {
  return (
    <>
      {header && <Header></Header>}
      <Container bgColor="gray">{children}</Container>
      {footer && <Footer></Footer>}
    </>
  );
};

Layout.defaultProps = {
  header: false,
  footer: false,
};

export default Layout;
