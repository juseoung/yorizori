import React from 'react';
import { SubHead } from '../Typography';

interface SectionProps {
  title?: string | boolean;
  titleColor: 'primary' | 'black' | 'white';
  sub?: string | boolean;
  bgColor?: 'white';
  children: React.ReactNode;
}

// const COLOR_OPTION = {
//   primary: 'text-primary-100',
//   black: 'text-black',
//   white: 'text-white',
// };
const BG_OPTION = {
  white: 'bg-white',
};

const Section = ({
  title,
  //   titleColor,
  sub,
  bgColor,
  children,
}: SectionProps) => {
  //   const colorClassName = titleColor ? COLOR_OPTION[titleColor] : '';
  const bgClassName = bgColor ? BG_OPTION[bgColor] : '';
  return (
    <>
      {title && (
        <div className="flex flex-row items-center">
          <SubHead>{title}</SubHead>
          <span>{sub}</span>
        </div>
      )}
      <div className={`rounded-lg ${bgClassName}`}>{children}</div>
    </>
  );
};

Section.defaultProps = {
  titleColor: 'black',
  bgColor: 'white',
};

export default Section;
