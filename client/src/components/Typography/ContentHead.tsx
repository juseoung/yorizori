import React from 'react';

interface ContentHeadProps {
  color?: 'black' | 'white';
  children?: React.ReactNode;
  className?: React.ReactNode;
}

const COLOR_OPTION = {
  black: 'text-black',
  white: 'text-white',
};

export const ContentHead = ({
  color,
  children,
  className,
}: ContentHeadProps) => {
  const colorClassName = color ? COLOR_OPTION[color] : '';

  return (
    <strong
      className={`${className} font-sans text-content-head font-bold ${colorClassName}`}
    >
      {children}
    </strong>
  );
};

// Spoqa Han Sans Neo
