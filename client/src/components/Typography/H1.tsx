import React from 'react';

interface H1Props {
  color?: 'primary' | 'secondary' | 'yellow' | 'black' | 'white';
  children?: React.ReactNode;
  className?: React.ReactNode;
}

const COLOR_OPTION = {
  primary: '',
  secondary: '',
  yellow: 'text-point-yellow',
  black: '',
  white: '',
};

export const H1 = ({ color, children, className }: H1Props) => {
  const colorClassName = color ? COLOR_OPTION[color] : '';
  return (
    <h1 className={`${className} text-h1 font-bold ${colorClassName}`}>
      {children}
    </h1>
  );
};

H1.defaultProps = {
  color: 'white',
};

// h1
// Font
// Spoqa Han Sans Neo
// Weight
// 700
// Style
// normal
// Size
// 40px
// Line height
// 52px
// Letter
// -0.6 px
