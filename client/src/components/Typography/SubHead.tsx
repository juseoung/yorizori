import React from 'react';

interface SubHeadProps {
  color?: 'primary' | 'black' | 'white';
  children?: React.ReactNode;
  className?: React.ReactNode;
}

const COLOR_OPTION = {
  primary: 'text-primary-100',
  black: 'text-black',
  white: 'text-white',
};

export const SubHead = ({ color, children, className }: SubHeadProps) => {
  const colorClassName = color ? COLOR_OPTION[color] : '';

  return (
    <strong
      className={`${className} font-sans text-sub-head font-bold ${colorClassName}`}
    >
      {children}
    </strong>
  );
};

SubHead.defaultProps = {
  color: 'black',
};

// Spoqa Han Sans Neo
