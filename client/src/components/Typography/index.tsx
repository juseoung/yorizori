import { H1 } from './H1';
import { SubHead } from './SubHead';
import { ContentHead } from './ContentHead';

export { SubHead, ContentHead };

export default H1;
