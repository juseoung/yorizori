import React from 'react';
import H1, { ContentHead, SubHead } from '../../components/Typography';

interface AssetValueProps {
  user?: string;
  value: number;
}

const AssetValue = ({ user, value }: AssetValueProps) => {
  return (
    <>
      <div className="flex flex-col items-end relative pt-[90px] pb-2 px-2 rounded-lg bg-primary-100">
        <div className="absolute left-[-24px] top-[30px]">
          <div className="py-1 pr-[20px] pl-[40px] rounded-lg bg-primary-60">
            <SubHead color="white">{user} 님</SubHead>
          </div>
        </div>

        <ContentHead className="label" color="white">
          총평가자산
        </ContentHead>
        <H1 color="yellow">₩ {value}</H1>

        <span>자세히 보기</span>
      </div>
    </>
  );
};

AssetValue.defaultProps = {
  user: '홍길동',
  value: 0,
};

export default AssetValue;
