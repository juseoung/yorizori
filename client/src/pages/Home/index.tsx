import React from 'react';
import Layout from '../../components/Layout';
import Section from '../../components/Section';
import AssetValue from '../AssetValue';
import MorningCall from '../MorningCall';

const Home = () => {
  return (
    <>
      <Layout>
        <AssetValue user="박동준" value={10000000}></AssetValue>

        <Section title="모닝콜" sub="매일 찾아오는 2가지 글로벌 이슈">
          <MorningCall />
        </Section>
      </Layout>
    </>
  );
};

export default Home;
