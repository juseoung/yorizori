module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['Helvetica'],
    },
    fontSize: {
      xs: [
        '14px',
        {
          letterSpacing: '-0.03em',
          lineHeight: '20px',
        },
      ],
      sm: '',
      tiny: '',
      base: '',
      lg: '',
      xl: '',
    },
    fontWeight: {
      light: '300',
      normal: '500',
      bold: '700',
    },
    colors: {
      transparent: 'transparent',
      primary: {
        100: '#5E4EDE',
        60: '#9E95EB',
      },
      secondary: {
        100: '#f00',
      },
      point: {
        yellow: '#F9CF68',
      },
      black: '#000000',
      white: '#ffffff',
      gray: {
        100: '',
        80: '',
        60: '#f4f4f4',
      },
    },
    spacing: {
      10: '2.5rem',
      20: '5rem',
    },
    borderRadius: {
      none: 0,
      sm: '',
      DEFAULT: '',
      lg: '16px',
      full: '9999px',
    },
    opacity: {
      0: 0,
      20: 0.2,
      40: 0.4,
    },
    extend: {
      fontSize: {
        h1: [
          '40px',
          {
            letterSpacing: '-0.3px',
            lineHeight: '52px',
          },
        ],
        'sub-head': [
          '16px',
          {
            letterSpacing: '-0.6px',
            lineHeight: '22px',
          },
        ],
        'content-head': [
          '14px',
          {
            letterSpacing: '-0.6px',
            lineHeight: '20px',
          },
        ],
      },
      spacing: {
        1: '8px',
        2: '16px',
        3: '24px',
        // 4: '30px',
      },
      screens: {
        '3xl': '1600px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
